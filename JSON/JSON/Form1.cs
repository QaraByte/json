﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Web.Script.Serialization;

namespace JSON
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string js = "";
        
        private void btnOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileTxt = new OpenFileDialog();

            if (fileTxt.ShowDialog() == DialogResult.OK)
                txtPath.Text = fileTxt.FileName;

            if (txtPath.Text != String.Empty)
            {
                string s2 = File.ReadAllText(txtPath.Text);
                txtJSON.Text = s2;
                js = s2;
                btnDeserialize.Enabled = true;
            }
            else
            {
                txtJSON.Text = "Файл не выбран.";
            }
        }

        private void btnDeserialize_Click(object sender, EventArgs e)
        {
            if (js != "" || txtJSON.Text != "")
            {
                txtJSON.Text = "";
                JavaScriptSerializer json = new JavaScriptSerializer();
                List<Object> objects = json.Deserialize<List<Object>>(js);
                List<Obj> res = new List<Obj>();
                foreach (object o in objects)
                {
                    res.Add(Create((Dictionary<string, Object>)o));
                    txtJSON.Text += "Хеш объекта (Индекс блока синхронизации): " +
                        o.GetHashCode().ToString() + Environment.NewLine;
                }

                Obj.objArray = res;
                Obj obj = new Obj();
                string[] items = obj.GetData();

                if (items.Length > 0)
                    foreach (string s in items)
                    {
                        txtJSON.Text += s + Environment.NewLine;
                    }
            }
            else
            {
                txtJSON.Text = "Текстовое поле пустое или формат текста не JSON. Нечего десериализовывать.";
            }
        }

        private static Obj Create(Dictionary<string, object> o)
        {
            Obj res = new Obj();
            res.name = (string)o["name"];
            res.countTraining = (int)o["countTrainig"];
            Object[] weightData = (Object[])o["weight"];
            int arrSize = (int)Math.Sqrt(weightData.Length);
            res.weight = new double[arrSize, arrSize];
            int index = 0;
            for (int n = 0; n < res.weight.GetLength(0); n++)
                for (int m = 0; m < res.weight.GetLength(1); m++)
                {
                    res.weight[n, m] = Double.Parse(weightData[index].ToString());
                    index++;
                }
            return res;
        }   
    }
}