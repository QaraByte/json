﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JSON
{
    class Obj
    {
        public string name;
        public double[,] weight;
        public int countTraining;

        public Obj() { }
        public string GetName() { return name; }

        public void Clear(string name, int x, int y)
        {
            this.name = name;
            weight = new double[x, y];
            for (int i = 0; i < weight.GetLength(0); i++)
                for (int j = 0; j < weight.GetLength(1); j++)
                    weight[i, j] = 0;
            countTraining = 0;
        }

      /*  public double GetRes(int[,] data)
        {
            if (weight.GetLength(0) != data.GetLength(0) || weight.GetLength(1) != data.GetLength(1))
                return -1;

            double res = 0;
            for (int i = 0; i < weight.GetLength(0); i++)
                for (int j = 0; j < weight.GetLength(1); j++)
                    res += 1 - Math.Abs(weight[i, i] - data[i, j]);
            return res / weight.GetLength(0) * weight.GetLength(1);
        }

        public int Training(int[,] data)
        {
            if (data == null || weight.GetLength(0) != data.GetLength(0) || weight.GetLength(1) != data.GetLength(1))
                return countTraining;
            countTraining++;
            for (int n = 0; n < weight.GetLength(0); n++)
                for (int m = 0; m < weight.GetLength(1); m++)
                {
                    double v = data[n, m] == 0 ? 0 : 1;
                    weight[n, m] += 2 * (v - 0.5f) / countTraining;
                    if (weight[n, m] > 1)
                        weight[n, m] = 1;
                    if (weight[n, m] < 0)
                        weight[n, m] = 0;
                }
            return countTraining;
        }*/

        public static List<Obj> objArray = null;

        public string[] GetData()
        {
            var res = new List<string>();
            if (objArray != null)
                for (int i = 0; i < objArray.Count; i++)
                    res.Add(objArray[i].GetName());
            res.Sort();
            return res.ToArray();
        }
    }
}
